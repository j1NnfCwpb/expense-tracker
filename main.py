import db_funcs
from getpass import getpass
from hashlib import sha256
from sys import exit as sys_exit
 
OPERATIONS = ["Add transaction",
              "Edit transaction",
              "Delete transaction",
              "List filtered transactions",
              "Exit"
             ]
NUMBER_OPS = len(OPERATIONS)

def login():

    warn_msgs = [" "*4 + "Second attempt.",
                 " "*4 + "Third and last attempt.",
                 " "*4 + "Closing application."]

    with open("passwd", 'r') as f:
        stored_passwd_hash = f.read()
        attempts = 1
        while attempts < 4:
            passwd = getpass("\nPassword: ")
            passwd_hash = sha256(passwd.encode('UTF-8')).hexdigest()
            if stored_passwd_hash.strip('\n') == passwd_hash:
                print()
                break
            print("[!] Wrong password inserted.")
            print(warn_msgs[attempts - 1])
            attempts += 1
            if attempts == 4:
                sys_exit()

def menu():

    while True:

        print("*"*37)
        print("*"*10 + " Expense Tracker " + "*"*10)
        print("*"*37, end="\n\n")
        for op_id, op in enumerate(OPERATIONS):
            print(f"{op_id}) {op}")
        print()

        selected_op = input("#> ")

        try:
            number = int(selected_op)
            if 0 <= number < NUMBER_OPS:
                return number

        except ValueError:
            pass

        except Exception as e:
            print(e)
        
        print(f"[!] Only numbers between 0 and {NUMBER_OPS}.", end="\n\n")

def execute_query(con, query):
    cur = con.cursor()
    res = cur.execute(query)
    tmp = res.fetchall()
    cur.close()
    return tmp

def list_transactions(con):

    tmp = execute_query(con=con, query="SELECT * FROM transactions")

    print("ID\t|Data\t|Amount\t|Category\t|Involved\t|Scale\t|Description")
    for id, date, amount, category, involved, scale, description in tmp:
        row = "%4d | %-12s|%8.2f | %-20s| %-20s|%3d | %s" % (id, date, amount, category, involved, scale, description)
        print(row)

def edit(con):

    try:
        id = int(input("Transaction ID: "))
        tmp = execute_query(con, "SELECT * FROM transactions WHERE id = %d" % (id))
        
        edit_list = ['id', 'date', 'amount', 'category', 'involved', 'scale', 'description']
        for i, option in enumerate(edit_list):
            input(f"\t {option[i]} (current value: {tmp[i+1]}): ")
        
    except ValueError:
        print("[!] Invalid ID inserted.")
        print(" "*4 + "Please use only numbers.")
    except Exception as e:
        print(e)

if __name__ == '__main__':
    
    con = db_funcs.dbg_db()

    # login()

    while True:
        op = menu()
        if op == 0:
            # Add
            pass
        elif op == 1:
            # Edit
            edit(con)
        elif op == 2:
            # Delete
            pass
        elif op == 3:
            # List
            list_transactions(con)
        else:
            # Close
            exit()